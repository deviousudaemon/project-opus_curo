extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var is_favorited : bool = false setget set_is_favorited

onready var favorite_icon_true = find_node("FavoriteIconTrue")
onready var favorite_icon_false = find_node("FavoriteIconFalse")

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	set_is_favorited(is_favorited)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func set_is_favorited(var is_true : bool):
	if is_true:
		if !favorite_icon_true.visible: favorite_icon_true.visible = true
		if favorite_icon_false.visible: favorite_icon_false.visible = false
	else:
		if favorite_icon_true.visible: favorite_icon_true.visible = false
		if !favorite_icon_false.visible: favorite_icon_false.visible = true
	pass