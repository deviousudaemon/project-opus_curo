extends CheckButton

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var text_color_normal : Color = Color("ffffff")
var text_color_dark : Color = Color("a0a0a0")
var scene_type : int = 0

onready var two_d_label := find_node("2DLabel") as Label
onready var three_d_label := find_node("3DLabel") as Label

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	connect("toggled", self, "_on_toggled")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_toggled(var button_pressed):
	# false == private
	# true == public
	if button_pressed:
		if scene_type == 0: scene_type = 1
		if two_d_label.modulate != text_color_dark: two_d_label.modulate = text_color_dark
		if three_d_label.modulate != text_color_normal: three_d_label.modulate = text_color_normal
	else:
		if scene_type == 1: scene_type = 0
		if two_d_label.modulate != text_color_normal: two_d_label.modulate = text_color_normal
		if three_d_label.modulate != text_color_dark: three_d_label.modulate = text_color_dark
	pass