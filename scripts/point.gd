extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var list
var scene_manager
var point_name = ""
signal point_name_changed(new_name)
signal list_name_changed(new_name)
signal ready_complete
export var has_init : bool


onready var rename_point_edit = find_node("RenamePointEdit", true, false)
onready var enter_point_name = find_node("EnterPointName", true, false)
onready var edit_point_button = find_node("EditPointButton", true, false)
onready var edit_point_button_text = find_node("PointNameLabel", true, false)
onready var confirm_point_deletion = find_node("ConfirmPointDeletion", true, false)
onready var default_point_text : String = enter_point_name.text
onready var confirm_generic_name = find_node("ConfirmGenericName", true, false)
onready var warning_name_too_short = find_node("WarningNameTooShort", true, false)
onready var point_properties = find_node("PointProperties", true, false)
onready var input_blocker = find_node("InputBlocker", true, false)


var default_point_name : String = "Point"

var entering_name : bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	filename = ""
	if get_parent() != null:
		if get_parent().get_parent() != null:
			list = get_parent().get_parent().get_parent()
			list.connect("list_name_changed", self, "on_list_name_changed")
	if !has_init:
		enter_name()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_creation():
	emit_signal("ready_complete")
	pass

func _on_enter_name_action():
	if enter_point_name.text == default_point_text:
		if !confirm_generic_name.visible:
			confirm_generic_name.visible = true
			confirm_generic_name.focus_mode = Control.FOCUS_ALL
#			toggle_input_blocker()
	else:
		if check_valid_name(enter_point_name.text):
			if enter_point_name.visible:
				enter_point_name.visible = false
			point_name = enter_point_name.text
			emit_signal("point_name_changed", point_name)
			edit_point_button_text.text = point_name
			rename_point_edit.text = point_name
		else:
			warning_name_too_short.visible = true
			enter_point_name.text = default_point_name
	entering_name = false
	has_init = true
	pass

func on_list_name_changed(new_name):
	emit_signal("list_name_changed", new_name)
	pass

func _on_enter_rename():
	if check_valid_name(rename_point_edit.text):
		edit_point_button_text.text = rename_point_edit.text
		rename_point_edit.placeholder_text = rename_point_edit.text
		rename_point_edit.visible = false
	else:
		warning_name_too_short.visible = true
		rename_point_edit.text = rename_point_edit.placeholder_text
	pass

func _on_cancel_rename():
	rename_point_edit.text = rename_point_edit.placeholder_text
	rename_point_edit.visible = false
	pass

func check_valid_name(var text: String):
	if text.length() > 0:
		var newText = text.replace(" ", "")
		if newText.length() > 0:
			return true
		else:
			return false
	else:
		return false
	pass

func enter_name():
	entering_name = true
	if !enter_point_name.visible:
		enter_point_name.visible = true
	enter_point_name.grab_focus()
	pass

func _on_RenamePointButton_pressed():
	if !rename_point_edit.visible:
		rename_point_edit.visible = true
		rename_point_edit.grab_focus()
		rename_point_edit.select_all()
	pass # Replace with function body.


func _on_DeletePointButton_pressed():
#	toggle_input_blocker()
	if !confirm_point_deletion.visible:
		confirm_point_deletion.visible = true
	confirm_point_deletion.focus_mode = Control.FOCUS_ALL
	pass # Replace with function body.


func _on_ConfirmPointDeletion_confirmed():
	queue_free()
	pass # Replace with function body.


func _on_AcceptGenericName_pressed():
#	toggle_input_blocker()
	if !list.has_points():
		point_name = default_point_name
		emit_signal("point_name_changed", point_name)
		edit_point_button_text.text = point_name
		rename_point_edit.placeholder_text = point_name
		rename_point_edit.text = point_name
		enter_point_name.visible = false
		confirm_generic_name.visible = false
	else:
		point_name = default_point_name + String(list.get_point_count() - 1)
		emit_signal("point_name_changed", point_name)
		edit_point_button_text.text = point_name
		rename_point_edit.placeholder_text = point_name
		rename_point_edit.text = point_name
		enter_point_name.visible = false
		confirm_generic_name.visible = false
	pass # Replace with function body.


func _on_RejectGenericName_pressed():
#	toggle_input_blocker()
	confirm_generic_name.visible = false
	pass # Replace with function body.


func _on_ConfirmPointDeletion_cancel():
#	toggle_input_blocker()
	if confirm_point_deletion.visible:
		confirm_point_deletion.visible = false
	confirm_point_deletion.focus_mode = Control.FOCUS_NONE
	pass # Replace with function body.


func _on_EditPointButton_pressed():
	point_properties.visible = true
#	toggle_input_blocker()
	pass # Replace with function body.


func _on_PointMarginContainer_resized(var node):
	if rect_min_size != node.rect_size:
		rect_min_size = node.rect_size
	pass # Replace with function body.

#func toggle_input_blocker():
#	if !input_blocker.visible: input_blocker.visible = true
#	else: input_blocker.visible = false
#	pass
