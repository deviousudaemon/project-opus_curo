extends LineEdit

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var point = get_parent()

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	get_parent().connect("ready_complete", self, "_on_ready_complete")
	connect("visibility_changed", self, "_on_visibility_changed")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if has_focus():
		if Input.is_action_just_released("ui_accept"):
			point._on_enter_name_action()
			pass
		elif Input.is_action_just_released("ui_cancel"):
			release_focus()
			pass
	pass

func _on_ready_complete():
	owner = get_parent().owner
	pass

func _on_visibility_changed():
	if visible:
		set_process(true)
	else:
		set_process(false)
	pass