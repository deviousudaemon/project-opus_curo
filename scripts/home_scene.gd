extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var node_type = "home"

const minimum_window_size = Vector2(266, 318)
const scale_method : int = 1

onready var settings_menu := find_node("SettingsMenu") as Popup
onready var new_project_menu := find_node("NewProjectMenu") as Popup
onready var browse_projects_menu := find_node("BrowseProjectsMenu") as Popup

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	get_tree().get_root().connect("size_changed", self, "viewport_size_changed")
	set_size_scaled(scale_method)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func viewport_size_changed():
	set_size_scaled(scale_method)
	pass

func set_size_scaled(var method: int):
	match method:
		0:
			var scaling_factor_x = get_viewport_rect().size.x / rect_size.x
			var scaling_factor_y = get_viewport_rect().size.y / rect_size.y
			if rect_scale.x != scaling_factor_x: rect_scale.x = scaling_factor_x
			if rect_scale.y != scaling_factor_y: rect_scale.y = scaling_factor_y
		1:
			var scaled_size = Vector2()
			scaled_size.x = get_viewport_rect().size.x
			scaled_size.y = get_viewport_rect().size.y
			if rect_size.x != scaled_size.x: rect_size.x = scaled_size.x
			if rect_size.y != scaled_size.y: rect_size.y = scaled_size.y
	pass

func _on_SettingsButton_pressed():
	settings_menu.visible = true
	pass # Replace with function body.


func _on_CreateProjectButton_pressed():
	new_project_menu.visible = true
	pass # Replace with function body.


func _on_ExitButton_pressed():
	get_tree().quit()
	pass # Replace with function body.


func _on_BrowseProjectsButton_pressed():
	browse_projects_menu.visible = true
	pass # Replace with function body.
