extends ConfirmationDialog

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal cancel
var cancel_button
var active = false
var has_confirmed = false

onready var center_position : Vector2 = (get_viewport_rect().size / 2) - (rect_size / 2)



# Called when the node enters the scene tree for the first time.
func _ready():
	if rect_position != center_position: rect_position = center_position
	set_process(false)
	get_parent().connect("ready_complete", self, "_on_ready_complete")
	connect("visibility_changed", self, "_on_visibility_changed")
	cancel_button = get_cancel()
	cancel_button.connect("pressed", self, "_on_cancel_pressed")
	get_viewport().connect("size_changed", self, "_on_viewport_size_changed")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if visible:
		if !active: active = true
		if Input.is_action_just_released("ui_accept"):
			emit_signal("confirmed")
			has_confirmed = true
		elif Input.is_action_just_released("ui_cancel"):
			emit_signal("cancel")
			has_confirmed = true
	else:
		if active:
			if has_confirmed: 
				active = false
				has_confirmed = false
				set_process(false)
			else:
				emit_signal("cancel")
				active = false
				set_process(false)
	pass

func _on_ready_complete():
	owner = get_parent().owner
	pass

func _on_visibility_changed():
	if visible:
		set_process(true)
	pass

func _on_cancel_pressed():
	emit_signal("cancel")
	has_confirmed = true
	pass

func _on_viewport_size_changed():
	center_position = (get_viewport_rect().size / 2) - (rect_size / 2)
	if rect_position != center_position: rect_position = center_position
	pass