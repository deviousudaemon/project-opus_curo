extends LineEdit

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var has_completed : bool = false

onready var list = get_parent().get_parent().get_parent()

# Called when the node enters the scene tree for the first time.
func _ready():
	get_parent().get_parent().get_parent().connect("ready_complete", self, "_on_ready_complete")
	set_process(false)
	connect("visibility_changed", self, "_on_visibility_changed")
	connect("focus_exited", self, "_on_focus_exited")
	if visible: visible = false
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if has_focus():
		if Input.is_action_just_released("ui_accept"):
			list.set_list_name(text)
			visible = false
			if !has_completed: has_completed = true
			release_focus()
			text = ""
			set_process(false)
		if Input.is_action_just_released("ui_cancel"):
			visible = false
			if !has_completed: has_completed = true
			release_focus()
			set_process(false)
			text = ""
	pass

func _on_ready_complete():
	if owner != get_parent().get_parent().get_parent().owner : owner = get_parent().get_parent().get_parent().owner
	pass

func _on_visibility_changed():
	if visible: 
		set_process(true)
		grab_focus()
		select_all()
	else: set_process(false)
	pass

func _on_focus_exited():
	if !has_completed:
		list.set_list_name(text)
		visible = false
		set_process(false)
		text = ""
		pass
	else:
		has_completed = false
	pass