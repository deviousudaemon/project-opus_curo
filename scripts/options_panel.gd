extends PopupPanel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var list_parent = get_parent()

# Called when the node enters the scene tree for the first time.
func _ready():
	get_parent().connect("ready_complete", self, "_on_ready_complete")
#	owner = get_parent()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if visible:
		if rect_position != list_parent.panel_position.helper_position():
			rect_position = list_parent.panel_position.helper_position()
		pass
	pass

func _on_ready_complete():
	if owner != get_parent().owner : owner = get_parent().owner
	pass