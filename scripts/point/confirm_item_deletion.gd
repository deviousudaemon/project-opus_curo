extends PopupPanel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var deletion_queue : Array = []
var queue_limit : int = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	get_parent().get_parent().connect("ready_complete", self, "_on_ready_complete")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_ready_complete():
	owner = get_parent().get_parent().owner
	pass

func queue_deletion(calling_object):
	if deletion_queue.size() < queue_limit:
		deletion_queue.append(calling_object)
		popup()
	pass

func _on_ConfirmButton_pressed():
	if !deletion_queue.empty():
		deletion_queue[0]._on_confirm_deletion(true)
		deletion_queue.clear()
		visible = false
		pass
	pass # Replace with function body.


func _on_RejectButton_pressed():
	confirm_deletion(false)
	pass # Replace with function body.


func confirm_deletion(is_true):
	if !deletion_queue.empty():
		deletion_queue[0]._on_confirm_deletion(is_true)
		deletion_queue.clear()
		visible = false
		pass
	pass
