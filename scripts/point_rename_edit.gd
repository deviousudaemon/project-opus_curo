extends LineEdit

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var point = get_parent()

var has_completed : bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	get_parent().connect("ready_complete", self, "_on_ready_complete")
	connect("visibility_changed", self, "_on_visibility_changed")
	connect("focus_exited", self, "_on_focus_exited")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if has_focus():
		if Input.is_action_just_released("ui_accept"):
			point._on_enter_rename()
			release_focus()
			has_completed = true
		elif Input.is_action_just_released("ui_cancel"):
			point._on_cancel_rename()
			release_focus()
			has_completed = true
	pass

func _on_ready_complete():
	owner = get_parent().owner
	pass

func _on_visibility_changed():
	if visible:
		set_process(true)
	else:
		set_process(false)
	pass

func _on_focus_exited():
	if has_completed:
		has_completed = false
	else:
		point._on_enter_rename()
	pass