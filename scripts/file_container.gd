extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var file_path : String
var browser_node

onready var select_file_button = $SelectFileButton

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func set_init_values(project_name, project_browser_node, path_to_file):
	select_file_button.text = project_name
	browser_node = project_browser_node
	file_path = path_to_file
	pass

func _on_SelectFileButton_pressed():
	browser_node.load_project(file_path)
	pass # Replace with function body.


func _on_DeleteFileButton_pressed():
	browser_node.ask_to_delete_file(file_path, self)
	pass # Replace with function body.
