extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var input_blocker = find_node("InputBlocker")

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func toggle_input_blocker():
	if input_blocker.visible: 
		input_blocker.visible = false
		print(false)
	else: 
		input_blocker.visible = true
		print(true)
	pass
