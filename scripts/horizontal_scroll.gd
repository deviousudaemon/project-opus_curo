extends ScrollContainer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var root = get_tree().get_root().find_node("root")

# Called when the node enters the scene tree for the first time.
func _ready():
	connect("gui_input", self, "_on_gui_input")
	get_v_scrollbar().rect_size.y *= 3.0
	if get_node("_h_scroll") != null: get_node("_h_scroll").visible = false
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_gui_input(var event: InputEvent):
	if event.is_action("ui_mouse_left"):
		if event is InputEventMouseButton and event.doubleclick:
			#Open "Add List?" menu at mouse position
			#hide if not in focus
			pass
	pass