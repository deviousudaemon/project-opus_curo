extends Popup

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var center_position : Vector2 = (get_viewport_rect().size / 2) - (rect_size / 2)

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	if rect_position != center_position: rect_position = center_position
	get_viewport().connect("size_changed", self, "_on_viewport_size_changed")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_viewport_size_changed():
	center_position = (get_viewport_rect().size / 2) - (rect_size / 2)
	if rect_position != center_position: rect_position = center_position
	pass

func _on_CloseButton_pressed():
	visible = false
	pass # Replace with function body.
