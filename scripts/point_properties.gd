extends PopupPanel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var point_checklist_prefab = preload("res://prefabs/point_prefabs/point_checklist.tscn")

onready var point = get_parent()
onready var point_name_label = $Control/Control3/PointItemContainer/MainItem/PointNameLabel
onready var point_list_name_label = $Control/Control3/PointItemContainer/MainItem/PointListNameLabel
onready var point_item_container = find_node("PointItemContainer", true, true) as VBoxContainer
onready var confirm_item_deletion = $ConfirmItemDeletion

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	get_parent().connect("ready_complete", self, "_on_ready_complete")
	get_parent().connect("point_name_changed", self, "_on_point_name_changed")
	get_parent().connect("list_name_changed", self, "_on_list_name_changed")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_ready_complete():
	owner = get_parent().owner
	point_name_label.text = point.point_name
	point_list_name_label.text = point.list.list_name
	pass

func _on_point_name_changed(new_name):
	point_name_label.text = new_name
	pass

func _on_list_name_changed(new_name):
	point_list_name_label.text = new_name
	pass

func _on_CloseButton_pressed():
	visible = false
	pass # Replace with function body.

func _on_ChecklistButton_pressed():
	var new_checklist = point_checklist_prefab.instance()
	point_item_container.add_child(new_checklist)
	point_item_container.move_child(new_checklist, 1)
	new_checklist.owner = owner
	new_checklist.point_properties_panel_node = get_path()
	new_checklist.confirm_item_deletion_node = confirm_item_deletion.get_path()
	new_checklist._on_creation()
	pass # Replace with function body.
