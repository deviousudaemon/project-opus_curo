extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var point_prefab = preload("res://prefabs/point.res")
var scene_manager

export var points : Array = []

signal ready_complete
signal list_name_changed(new_name)

var root
var list_name_node
var edit_name_button
var list_name
var list_options_panel
var panel_position
var point_container
var confirm_list_deletion
var list_name_edit
var input_blocker

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	filename = ""
	root = get_tree().get_root().find_node("root", true, false)
	list_name_node = find_node("NameText", true)
	edit_name_button = find_node("EditNameButton", true)
	list_name = list_name_node.text
	list_options_panel = find_node("OptionsPanel", true)
	panel_position = find_node("PanelPosition", true)
	point_container = find_node("PointContainer", true)
	confirm_list_deletion = find_node("ConfirmListDeletion", true)
	list_name_edit = find_node("NameEdit", true)
	input_blocker = find_node("InputBlocker", true)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_creation():
	emit_signal("ready_complete")
#	if owner != null:
#		for a in get_children():
#			if a.owner != null:
#				if a.owner != owner: a.owner = owner
#			if a.get_children().size() > 0:
#				for b in a.get_children():
#					if b.owner !=null:
#						if b.owner != owner: b.owner = owner
#					if b.get_children().size() > 0:
#						for c in b.get_children():
#							if c.owner !=null:
#								if c.owner != owner: c.owner = owner
#							if c.get_children().size() > 0:
#								for d in c.get_children():
#									if d.owner !=null:
#										if d.owner != owner: d.owner = owner
#									if d.get_children().size() > 0:
#										for e in d.get_children():
#											if e.owner !=null:
#												if e.owner != owner: e.owner = owner
	pass


func has_points():
	if points.size() > 1: return true
	else: return false
	pass

func get_point_count():
	return points.size()
	pass

func set_list_name(var new_name):
	if !edit_name_button.visible: edit_name_button.visible = true
	list_name_node.text = new_name
	list_name = new_name
	emit_signal("list_name_changed", new_name)
	list_name_edit.placeholder_text = new_name
	pass

func _on_ListOptionsButton_pressed():
	if list_options_panel.visible == true: list_options_panel.visible = false
	else: list_options_panel.visible = true
	pass # Replace with function body.


func _on_AddListPoint_pressed():
	var new_point = point_prefab.instance()
	points.append(new_point)
	point_container.add_child(new_point)
	new_point.owner = owner
	new_point._on_creation()
	var new_node_position = clamp(point_container.get_child_count() - 2, 0, point_container.get_child_count() - 2)
	point_container.move_child(new_point, new_node_position)
	pass # Replace with function body.


func _on_RenameList_pressed():
	edit_name_button.visible = false
	list_name_edit.visible = true
	pass # Replace with function body.


func _on_DeleteList_pressed():
	if !confirm_list_deletion.visible:
		confirm_list_deletion.visible = true
	confirm_list_deletion.grab_focus()
	pass # Replace with function body.


func _on_ConfirmListDeletion_cancel():
	if confirm_list_deletion.visible:
		confirm_list_deletion.visible = false
	pass # Replace with function body.


func _on_ConfirmListDeletion_confirmed():
	root.delete_list(self)
	pass # Replace with function body.

