extends PopupPanel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

const default_list_name = "List"

onready var center_position : Vector2 = (get_viewport_rect().size / 2) - (rect_size / 2)
onready var root = get_tree().get_root().find_node("root", true, false)
onready var enter_name = find_node("EnterName")
onready var input_blocker = get_parent().find_node("InputBlocker")


# Called when the node enters the scene tree for the first time.
func _ready():
	get_viewport().connect("size_changed", self, "_on_viewport_size_changed")
	connect("visibility_changed", self, "_on_visiblity_changed")
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if rect_position != center_position: rect_position = center_position
	if enter_name.has_focus():
		if Input.is_action_just_released("ui_accept"):
			_on_Accept_pressed()
		elif Input.is_action_just_released("ui_cancel"):
			_on_Reject_pressed()
	pass

func _on_visiblity_changed():
	if visible:
		if !input_blocker.visible:
			input_blocker.visible = true
		set_process(true)
	else:
		if input_blocker.visible: input_blocker.visible = false
		set_process(false)
	pass


func _on_Accept_pressed():
	root._on_EnterName_accepted()
	pass



func _on_Reject_pressed():
	root.cancel_create_list()
	pass # Replace with function body.

func _on_viewport_size_changed():
	center_position = (get_viewport_rect().size / 2) - (rect_size / 2)
	if rect_position != center_position: rect_position = center_position
	pass
