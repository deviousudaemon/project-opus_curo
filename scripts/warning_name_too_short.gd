extends AcceptDialog

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var list = get_parent()
onready var input_blocker = get_parent().find_node("InputBlocker")
onready var center_position : Vector2 = (get_viewport_rect().size / 2) - (rect_size / 2)

# Called when the node enters the scene tree for the first time.
func _ready():
	get_parent().connect("ready_complete", self, "_on_ready_complete")
	if rect_position != center_position: rect_position = center_position
	set_process(false)
	connect("visibility_changed", self, "_on_visibility_changed")
	connect("confirmed", self, "_on_confirmed")
	get_viewport().connect("size_changed", self, "_on_viewport_size_changed")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if has_focus():
		if Input.is_action_just_released("ui_accept") || Input.is_action_just_released("ui_cancel"):
			_on_confirmed()
			pass
	pass

func _on_ready_complete():
	owner = get_parent().owner
	pass

func _on_visibility_changed():
	if visible:
		set_process(true)
		input_blocker.queue_visible(self)
		grab_focus()
	else:
		set_process(false)
		release_focus()
	pass

func _on_confirmed():
	pass

func _on_viewport_size_changed():
	center_position = (get_viewport_rect().size / 2) - (rect_size / 2)
	if rect_position != center_position: rect_position = center_position
	pass