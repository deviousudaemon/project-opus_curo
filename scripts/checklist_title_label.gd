extends Label

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var font = get("custom_fonts/font") as Font

# Called when the node enters the scene tree for the first time.
func _ready():
	set_process(false)
	get_parent().get_parent().connect("ready_complete", self, "_on_ready_complete")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_ready_complete():
	owner = get_parent().get_parent().owner
	pass

func set_text_custom(new_text : String):
	var width = font.get_string_size(new_text).x
	if rect_size.x < width:
		rect_size.x = width
	elif rect_size.x > width:
		if width >= rect_min_size.x:
			rect_size.x = width
		else:
			rect_size.x = rect_min_size.x
	text = new_text
	pass