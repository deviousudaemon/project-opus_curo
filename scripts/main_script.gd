extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

const BASE_SIZE = Vector2(1024, 600)
var scale_method = 1
var node_type = "project"
var scene_manager = null
const minimum_window_size = Vector2(266, 318)

var lists = []

export var project_name : String = ""
export var project_icon : ImageTexture = null
export var project_is_private : bool = true
export var is_favorited : bool = false
export var scene_type : int

#warning-ignore:unused_class_variable
onready var project_name_label = find_node("ProjectNameLabel")
onready var project_icon_rect = find_node("ProjectIconRect")
onready var private_or_public = find_node("ProjectPrivateOrPublic")
onready var project_favorited = find_node("ProjectFavorited")
onready var list_container = find_node("ListContainer")
onready var ui_prompts = find_node("UIPrompts")
onready var create_list_prompt = find_node("CreateList")
onready var list_horizontal_container = find_node("ListHorizontalContainer")
onready var confirm_generic_name = find_node("ConfirmGenericName")
onready var list_enter_name = find_node("EnterName")
onready var default_list_name = list_enter_name.text

var default_list_text = "List"

# Prefabs
var prefab_vertical_seperator = preload("res://prefabs/vertical_seperator.res")
var prefab_list = preload("res://prefabs/list.res")

# Polish Variables
var size_has_changed_x = false
var size_has_changed_y = false
var size_changed_timer : float = 0.00
var size_changed_tick : float = 0.33

func _ready():
	#warning-ignore:return_value_discarded
	if scene_manager == null:
		scene_manager = get_node("/root/SceneManager")
	set_process(false)
	get_tree().get_root().connect("size_changed", self, "viewport_size_changed")
	set_size_scaled(scale_method)
	
	for a in get_children():
		if a.owner != self: a.owner = self
		if a.get_children().size() > 0:
			for b in a.get_children():
				if b.owner != self: b.owner = self
				if b.get_children().size() > 0:
					for c in b.get_children():
						if c.owner != self: c.owner = self
						if c.get_children().size() > 0:
							for d in c.get_children():
								if d.owner != self: d.owner = self
								if d.get_children().size() > 0:
									for e in d.get_children():
										if e.owner != self: e.owner = self
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_creation():
	project_name_label.text = project_name
	project_icon_rect.texture = project_icon
	if private_or_public.is_private != project_is_private:
		private_or_public.is_private = project_is_private
	if project_favorited.is_favorited != is_favorited:
		project_favorited.is_favorited = is_favorited
	pass

func viewport_size_changed():
	set_size_scaled(scale_method)
	pass

func set_size_scaled(var method: int):
	match method:
		0:
			var scaling_factor_x = get_viewport_rect().size.x / rect_size.x
			var scaling_factor_y = get_viewport_rect().size.y / rect_size.y
			if rect_scale.x != scaling_factor_x: rect_scale.x = scaling_factor_x
			if rect_scale.y != scaling_factor_y: rect_scale.y = scaling_factor_y
		1:
			var scaled_size = Vector2()
			scaled_size.x = get_viewport_rect().size.x
			scaled_size.y = get_viewport_rect().size.y
			if rect_size.x != scaled_size.x: rect_size.x = scaled_size.x
			if rect_size.y != scaled_size.y: rect_size.y = scaled_size.y
	pass

func get_prefab_list():
	return prefab_list
	pass

func has_list():
	if lists.size() > 0: return true
	else: return false
	pass

func _create_list_prompt():
	var create_list_prompt_name = create_list_prompt.find_node("EnterName")
	if create_list_prompt.visible != true:
		create_list_prompt.visible = true
	create_list_prompt_name.grab_focus()
	pass

func _on_EnterName_accepted():
	if list_enter_name.text == default_list_name:
		if not confirm_generic_name.visible:
			confirm_generic_name.visible = true
			confirm_generic_name.grab_focus()
	else:
		create_new_list(list_enter_name.text)
	pass

func _on_AcceptGenericName_pressed():
	if !has_list():
		create_new_list(default_list_text)
		if confirm_generic_name.visible:
			confirm_generic_name.visible = false
	else:
		create_new_list(default_list_text + String(get_list_count()))
		if confirm_generic_name.visible:
			confirm_generic_name.visible = false
	pass

func _on_EnterName_rejected():
	cancel_create_list()
	pass # Replace with function body.

func _on_RejectGenericName_pressed():
	if confirm_generic_name.visible:
		confirm_generic_name.visible = false
	pass # Replace with function body.

func create_new_list(var list_name):
	var seperator = prefab_vertical_seperator.instance()
	var new_list = prefab_list.instance(1)
	list_horizontal_container.add_child(seperator)
	seperator.owner = self
	list_horizontal_container.add_child(new_list)
	new_list.owner = self
	new_list._on_creation()
	var new_node_position_list = clamp(list_horizontal_container.get_child_count() - 3, 0, list_horizontal_container.get_child_count() - 3)
	list_horizontal_container.move_child(new_list, new_node_position_list)
	var new_node_position_seperator = clamp(list_horizontal_container.get_child_count() - 2, 0, list_horizontal_container.get_child_count() - 2)
	list_horizontal_container.move_child(seperator, new_node_position_seperator)
	new_list.set_list_name(list_name)
	lists.append(new_list)
	if create_list_prompt.visible == true:
		create_list_prompt.visible = false
	pass

func cancel_create_list():
	if create_list_prompt.visible == true:
		create_list_prompt.visible = false
	pass

func get_list_count():
	return lists.size()
	pass

func delete_list(var list):
	var list_position = list.get_index()
	var list_seperator = list.get_parent().get_child(list_position + 1)
	list.queue_free()
	list_seperator.queue_free()
	pass

func _on_HomeButton_pressed():
	scene_manager.goto_home_menu()
	pass # Replace with function body.
