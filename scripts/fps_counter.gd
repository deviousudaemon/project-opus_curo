extends Label

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	if visible: set_process(true)
	else: set_process(false)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	set_new_text(String(Engine.get_frames_per_second()), String(delta))
	pass


func set_new_text(var fps : String, var frame_time : String):
	text = fps + "\n" + frame_time
	pass
