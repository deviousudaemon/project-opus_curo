extends Button

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal started_hovering
signal stopped_hovering
var hovering : bool = false

onready var panel_normal = $PanelNormal
onready var panel_click = $PanelClick
onready var label = $Label
onready var new_project_menu = get_parent()

# Called when the node enters the scene tree for the first time.
func _ready():
	if label.visible : label.visible = false
	if panel_normal.visible : panel_normal.visible = false
	if panel_click.visible : panel_click.visible = false
	set_process(false)
	connect("started_hovering", self, "_on_started_hovering")
	connect("stopped_hovering", self, "_on_stopped_hovering")
	connect("visibility_changed", self, "_on_visibility_changed")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if is_hovered():
		if !hovering:
			emit_signal("started_hovering")
			hovering = true
		if pressed:
			if panel_normal.visible: panel_normal.visible = false
			if !panel_click.visible: panel_click.visible = true
		else:
			if !panel_normal.visible: panel_normal.visible = true
			if panel_click.visible: panel_click.visible = false
	else:
		if hovering:
			emit_signal("stopped_hovering")
			hovering = false
	pass

func _on_visibility_changed():
	if visible: set_process(true)
	else:
		if visible: visible = false
		set_process(false)
	pass

func _on_started_hovering():
	if !label.visible: label.visible = true
	if !pressed:
		if !panel_normal.visible: panel_normal.visible = true
	else:
		if panel_normal.visible: panel_normal.visible = false
		if !panel_click.visible: panel_click.visible = true
	pass

func _on_stopped_hovering():
	if label.visible: label.visible = false
	if panel_normal.visible: panel_normal.visible = false
	if panel_click.visible: panel_click.visible = false
	pass
